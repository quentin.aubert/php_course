<?php

class DndCharacter
{
  public function __construct()
  {
    $this->strength = $this->calcDice();
    $this->dexterity = $this->calcDice();
    $this->constitution = $this->calcDice();
    $this->intelligence = $this->calcDice();
    $this->wisdom = $this->calcDice();
    $this->charisma = $this->calcDice();
    $this->hitpoints = 10 + $this::modifier($this->constitution);
  }

  public static function modifier($value)
  {
    return floor(($value - 10) / 2);
  }

  public function calcDice()
  {
    return random_int(3, 18);
  }

  public static function ability()
  {
    return (new DndCharacter())->calcDice();
  }

  public static function generate()
  {
    return new DndCharacter();
  }
}
?>