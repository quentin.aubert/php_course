<?php 
class Clock
{
  private $hour;
  private $minute;
  
  public function __construct($hour = 0, $minute = 0)
  {
    $this->setTime($hour, $minute);
  }

  private function setTime($hour, $minute)
  {
      $this->h = (24 + ($hour + intval($minute / 60) + ($minute < 0 ? -1 : 0)) % 24) % 24;
      $this->m = (60 + $minute % 60) % 60;
  }

  public function __toString(): string
  {
    $fn = function ($x) {
        return ($x < 10) ? "0" . $x : $x;
    };
    return $fn($this->h) . ":" . $fn($this->m);
  }

  public function add($minute)
  {
    $this->setTime($this->h, $this->m + $minute);
    return $this;
  }
  
  public function sub($minute)
  {
    $this->setTime($this->h, $this->m - $minute);
    return $this;
  }
}
?>