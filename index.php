<?php

require "helloWorld.php";
require "reverseString.php";
require "robot.php";
require "maskify.php";
require "clock.php";
require "school.php";
require "matrix.php";

try {
    echo 'Current PHP version: ' . phpversion();
    echo '<br />';

    $host = 'db';
    $dbname = 'database';
    $user = 'user';
    $pass = 'pass';
    $dsn = "mysql:host=$host;dbname=$dbname;charset=utf8";
    $conn = new PDO($dsn, $user, $pass);

    echo 'Database connected successfully';
    echo '<br />';
} catch (\Throwable $t) {
    echo 'Error: ' . $t->getMessage();
    echo '<br />';
}

// Exercise 1
echo helloWorld();
echo '<br />';

// Exercise 2
echo reverseString('cool');
echo '<br />';

// Exercise 3
$robot = new Robot();
echo $robot->getName();
echo '<br />';

$robot2 = new Robot($robot->getName());
echo $robot2->getName();
echo '<br />';

// Exercise 4
echo maskify('123456789012'). '<br />';
echo maskify('1234-5678-9012') . '<br />';
echo maskify('') . '<br />';

// Exercise 5
$clock = new Clock(8);
echo $clock . '<br />';
$clock->add(2);
echo $clock . '<br />';
$clock->sub(4);
echo $clock . '<br />';

// Exercise 6
$school = new School();
$school->add('Zola', 1);
$school->add('Rémi', 1);
$school->add('Arachide', 1);
$school->add('Rémi', 12);
$school->add('Pierre', 1);
$school->add('Alexandre', 6);
$school->add('Rémi', 2);
// print_r($school->studentsByGradeAlphabetical());

// Exercise 7
$matrix = new Matrix("1 2 3 4 8\n4 5 6 4 7\n7 8 0 4 6\n7 8 0 4 5");
