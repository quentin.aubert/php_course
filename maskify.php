<?php

function maskify(string $cc): string
{
    $cc_length = strlen($cc);

    if (!$cc_length<6) {
        for ($i = 1; $i < $cc_length - 4; $i++) {
            if ($cc[$i] == '-') {
            
                continue;
            }
        $cc[$i] = '#';
        }
    }
    
    return $cc;
}