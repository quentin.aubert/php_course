<?php

class School
{
  protected $school = [];

  public function add(string $name, int $grade): void
  {
      $this->school[$grade][] = $name;
  }

  public function grade($grade)
  {
      return key_exists($grade, $this->school) ? $this->school[$grade] : null;
  }

  public function numberOfStudents()
  {
      return array_reduce($this->school, function ($acc, $item) {
          return $acc + count($item);
      }, 0);
  }

  public function studentsByGradeAlphabetical(): array
  {
    return array_map(function ($item) {
      sort($item);
      return $item;
    }, $this->school);
  }
}
?>